﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace week1_ex5
{
    class Program
    {
        static void Main(string[] args)
        {
            var a = 7;
            var b = 18;

            Console.WriteLine(a + b);

            Console.WriteLine($"{a} + {b} = {a + b}"); //chose this method as the layout is easy to read and displays calculation variables.

        }
    }
}
