﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace week1_ex8
{
    class Program
    {
        static void Main(string[] args)
        {
            var a = 0;
            var b = 0;
            var c = 0;
            var d = 0;
            var e = 0;

            Console.WriteLine("Please input 5 numbers:  ");

            a = int.Parse(Console.ReadLine());
            b = int.Parse(Console.ReadLine());
            c = int.Parse(Console.ReadLine());
            d = int.Parse(Console.ReadLine());
            e = int.Parse(Console.ReadLine());

            Console.WriteLine((a + b) * c / (d - e));
        }
    }
}
