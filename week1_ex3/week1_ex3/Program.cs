﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace week1_ex3
{
    class Program
    {
        static void Main(string[] args)
        {
            string name;

            Console.WriteLine("Hello, please type in your name:");
            name = Console.ReadLine();

            Console.WriteLine($"Thankyou {name}");

        }
    }
}
