﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace week1_ex9c
{
    class Program
    {
        static void Main(string[] args)
        {
            var r = 0;
            var pie = 3.14;

            Console.WriteLine("Please enter radius of your circle:  ");
            r = int.Parse(Console.ReadLine());

            Console.WriteLine("The area of your circle is:  ");
            Console.WriteLine(pie * r * r);


        }
    }
}
