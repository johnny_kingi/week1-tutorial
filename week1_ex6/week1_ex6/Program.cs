﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace week1_ex6
{
    class Program
    {
        static void Main(string[] args)
        {
            var kms_miles = 0.621371;
            var kms = 0;

            Console.WriteLine("Please enter Kilometers:  ");
            kms = int.Parse(Console.ReadLine());

            Console.WriteLine($"{kms} kilometers = {kms * kms_miles} miles");
        }
    }
}
