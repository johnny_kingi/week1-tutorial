﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace week1_ex9d
{
    class Program
    {
        static void Main(string[] args)
        {
            var pie = 3.14;
            var r = 0;

            Console.WriteLine("Please enter radius of circle:  ");
            r = int.Parse(Console.ReadLine());

            Console.WriteLine("The circumference of circle is: ");
            Console.WriteLine(2 * pie * r);

        }
    }
}
