﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace week1_ex6b
{
    class Program
    {
        static void Main(string[] args)
        {
            var miles_kms = 1.609344;
            var miles = 0;

            Console.WriteLine("Please enter Miles:  ");
            miles = int.Parse(Console.ReadLine());

            Console.WriteLine($"{miles} miles = {miles * miles_kms} kilometers");
        }
    }
}
